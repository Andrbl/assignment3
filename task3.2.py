import torch
import torchvision
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from torch import nn
import torch.nn as nn
import torch.nn.functional as F
from torchvision.transforms import ToTensor
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data.dataloader import DataLoader
from torch.utils.data import random_split
import sklearn
from skimage.feature import hog

# Transforming images in dataset
#imgTransform = torchvision.transforms.Compose([torchvision.transforms.Grayscale(num_output_channels=1), torchvision.transforms.ToTensor(), torchvision.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
# STL10 dataset

trainDataset = torchvision.datasets.STL10(root='./', split='train', download=False, transform=ToTensor())#( hog(temp2[0].permute(1, 2, 0), orientations=8, pixels_per_cell=(16, 16), cells_per_block=(1, 1), visualize=True, multichannel=True ),ToTensor()))
testDataset = torchvision.datasets.STL10(root='./', split='test', download=False, transform=ToTensor())#( hog(temp5[0].permute(1, 2, 0), orientations=8, pixels_per_cell=(16, 16), cells_per_block=(1, 1), visualize=True, multichannel=True ),ToTensor()))

trainLoader = DataLoader(dataset=trainDataset, batch_size=100, shuffle=True)#, num_workers=4, pin_memory=True)
testLoader = DataLoader(dataset=testDataset, batch_size=100*2)#, num_workers=4, pin_memory=True)

def accuracy(outputs, labels):
    x, predict = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(predict == labels).item() / len(predict))

class ModelCNN(nn.Module):
    def train(self, dataBatch):
        images, labels = dataBatch
        #hogData = hog(images.permute(1, 2, 0), orientations=8, pixels_per_cell=(16, 16), cells_per_block=(1, 1), visualize=False, feature_vector=True, multichannel=True)
        out = self(images)
        loss = F.cross_entropy(out, labels)
        return loss

    def testData(self, dataBatch):
        images, labels = dataBatch
        #hogData = hog(images, orientations=8, pixels_per_cell=(16, 16), cells_per_block=(1, 1), visualize=False, feature_vector=True, multichannel=True)
        out = self(images)
        loss = F.cross_entropy(out, labels)
        acc = accuracy(out, labels)
        return {"ModelLoss:" : loss, "ModelAccuracy:" : acc}

    def testDataLoop(self, outputs):
        batchDataLoss = [i["ModelLoss:"] for i in outputs]
        numRunsLoss = torch.stack(batchDataLoss).mean()
        batchDataAcc = [i["ModelAccuracy:"] for i in outputs]
        numRunsAcc = torch.stack(batchDataAcc).mean()
        return {"ModelLoss:" : numRunsLoss.item(), "ModelAccuracy:" : numRunsAcc.item()}

    def loopStop(self, numOfRuns, result):
        print("numOfRuns [{}], ModelLoss: {:.3f}, ModelAccuracy: {:.3f}".format(numOfRuns, result['ModelLoss:'], result['ModelAccuracy:']))

class stl10Model(ModelCNN):
   def __init__(self):
        super().__init__()
        self.network = nn.Sequential(            
            nn.Conv2d(3, 16, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),

            nn.Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2), # 16 * 8 * 8

            nn.Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),

            nn.Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),

            nn.Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),

            nn.Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),# 16 * 1 * 1

            nn.Flatten(), 
            nn.Linear(16, 10))

   def forward(self, xb):
        return self.network(xb)

cudaGpu = torch.device('cuda')   # Harcoded to use CUDA
print(cudaGpu)
print("Cuda is available: ", torch.cuda.is_available())

def dataToGpu(data, cudaGpu):
    if isinstance(data, (list,tuple)):
        return [dataToGpu(i, cudaGpu) for i in data]
    return data.to(cudaGpu, non_blocking=True)

class gpuDataLoader():
    def __init__(self, dataLoader, cudaGpu):
        self.dataLoader = dataLoader
        self.device = cudaGpu

    def __iter__(self):
        for i in self.dataLoader:
            yield dataToGpu(i, self.device)

    def __len__(self):
        return len(self.dataLoader)

@torch.no_grad()                            # Stops program from using 1 millon Gb of ram
def testDataCheckVal(model, testLoader):
    outputs = [model.testData(batchData) for batchData in testLoader]
    return model.testDataLoop(outputs)

def run(numOfRuns, learnR, model, trainLoader, testLoader, optFunc=torch.optim.Adam):
    log = []
    opti = optFunc(model.parameters(), learnR)
    for i in range(numOfRuns):
        for dataBatch in trainLoader:
            loss = model.train(dataBatch)
            loss.backward()
            opti.step()
            opti.zero_grad()
        result = testDataCheckVal(model, testLoader)
        model.loopStop(i, result)
        log.append(result)
    return log
 

trainLoader = gpuDataLoader(trainLoader, cudaGpu)
testLoader = gpuDataLoader(testLoader, cudaGpu)
model = dataToGpu(stl10Model(), cudaGpu)       # pass data to gpu

testDataCheckVal(model, testLoader)
log = run(15, 0.001, model, trainLoader, testLoader)