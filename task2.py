import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
from pprint import pprint
from hmmlearn import hmm
import math

def findMkEdge(x):
    edges = {}
    for col in x.columns:
        for idx in x.index:
            edges[(idx,col)] = x.loc[idx,col]
    return edges

#--------------------------------------------#
def printHMMvalues():   # Function prints out probability matrix
    hiddenStates = ['happy', 'sad']
    piHs = [0.6, 0.4]
    hDftest = pd.Series(piHs, index=hiddenStates, name='Hstates')
    print(hDftest)
    print('\n')
    hDf= pd.DataFrame(columns=hiddenStates, index=hiddenStates)
    hDf.loc[hiddenStates[0]] = [0.9, 0.1]
    hDf.loc[hiddenStates[1]] = [0.2, 0.8]
    print(hDf, '\n')

    observableStates = ['cooking', 'crying', 'sleeping', 'socializing', 'Watching TV']
    oDf = pd.DataFrame(columns=observableStates, index=hiddenStates)
    oDf.loc[hiddenStates[0]] = [0.1, 0.2, 0.4, 0.0, 0.3]
    oDf.loc[hiddenStates[1]] = [0.3, 0.0, 0.3, 0.3, 0.1]
    print('\nEmission prob', '\n', oDf)
    odfVal = oDf.values
    print('\n', odfVal, odfVal.shape, '\n')

    hsEdges = findMkEdge(hDf)
    pprint(hsEdges)
    osEdges = findMkEdge(oDf)
    pprint(osEdges)

def hmmCreate():
    print('\nCreating HMM\n')
    model = hmm.MultinomialHMM(n_components=2)
    model.startprob_ = np.array([0.6, 0.4])
    model.transmat_ = np.array([[0.9, 0.1],
                                [0.2, 0.8]])
    model.emissionprob_ = np.array([[0.1, 0.2, 0.4, 0.0, 0.3],
                                    [0.3, 0.0, 0.3, 0.3, 0.1]])
    return model

def hmmTask2():
    print('\nTask 2.2\n')
    model = hmmCreate()
    T = 20                 # Change this for a diffrent sequence    
    X, Z = model.sample(T)
    print(X)
    logP, sequence = model.decode(X)
    print(math.exp(logP))
    print(sequence)   


def hmmTask3():
    print('\nTask3\n')
    model = hmmCreate()
    T = 100                 # Change this for a diffrent sequence    
    X, Z = model.sample(T)
    print(X)
    logP, sequence = model.decode(X)
    print(sequence)
    a = sequence.size
    print(a)
    x = len(sequence)
    print(x)
    count1 = 0
    for x in sequence:
        if (x == 0):
            count1 += 1
    print(count1)
    if(count1 > 0):
        y = count1/a
        print("The probability of the user being in a happy state given a random sequence of events is: ", y)
    else:
        print("random sequence gave no happy user states, either increase T or run again")
    
    amountSeqW = 20              # Change this to change T
    arrayW = []
    for i in range(amountSeqW):
        arrayW.append(4)
    print('\n\n', arrayW)
    tempNParr = np.array([arrayW])
    #logP, sequence2 = model.predict([np.concatenate(tempNParr.transpose())])#.transpose())
    logP, sequence2 = model.decode(tempNParr.transpose())
    seq2numElem = sequence2.size
    j = len(sequence2)
    count2 = 0
    print('\nthis\n',sequence2)
    for j in sequence2:
        if(j == 0):
            count2 += 1
    if(count2 > 0):
        g = count2/seq2numElem
        print("The probability of the user being in a happy state given a X sequence of watching tv is: ", g)
    else:
        print("random sequence gave no happy user states, either increase T or run again")
            
def hmmTask4(): #Task 2.4 seq = {SO,SO,CO,W,SL} = {3, 3, 0, 4, 2}
    print('\nTask 2.4\n')
    model = hmmCreate()
    logP, sequence = model.decode(np.array([[3,3,0,4,2]]).transpose())
    print(math.exp(logP))
    print(sequence)



printHMMvalues()
hmmTask2()
hmmTask3()
hmmTask4()