import torch
import torchvision
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from torch import nn
import torch.nn as nn
import torch.nn.functional as F
from torchvision.transforms import ToTensor
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data.dataloader import DataLoader
from torch.utils.data import random_split

# Transforming images in dataset
#imgTransform = torchvision.transforms.Compose([torchvision.transforms.Grayscale(num_output_channels=1), torchvision.transforms.ToTensor(), torchvision.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# STL10 dataset
trainDataset = torchvision.datasets.STL10(root='./', split='train', download=False, transform=ToTensor())
#testDataset = torchvision.datasets.STL10(root='./', split='test', download=False, transform=ToTensor())

numOfData= 1000

trainDatasetSize = len(trainDataset) - numOfData
trainDataset, testDataset = random_split(trainDataset, [trainDatasetSize, numOfData])
len(trainDataset), len(testDataset)
# Data loader
trainLoader = DataLoader(dataset=trainDataset, batch_size=100, shuffle=True)#, num_workers=4, pin_memory=True)
#testLoader = DataLoader(dataset=testDataset, batch_size=numOfData)#, num_workers=4, pin_memory=True)
testLoader = DataLoader(dataset=testDataset, batch_size=100*2)#, num_workers=4, pin_memory=True)

def accuracy(outputs, labels):
    x, predict = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(predict == labels).item() / len(predict))

class stl10Model(nn.Module):
    def __init__(self, inSize, hiddenSize, outSize):
        super().__init__()
        self.HiddenLinear = nn.Linear(inSize, hiddenSize)       # Hidden layer
        self.OutLinear = nn.Linear(hiddenSize, outSize)         # Output layer

    def forward(self, xb):
        xb = xb.view(xb.size(0), -1)            # image tensor flatten
        out = self.HiddenLinear(torch.tanh(xb)) # get output hiddenlayer with tanh
        out = torch.sigmoid(out)                # sigmoid
        out = self.OutLinear(out)               # prediction output
        return out

    def train(self, dataBatch):
        images, labels = dataBatch
        out = self(images)
        loss = F.cross_entropy(out, labels)
        return loss

    def testData(self, dataBatch):
        images, labels = dataBatch
        out = self(images)
        loss = F.cross_entropy(out, labels)
        acc = accuracy(out, labels)
        return {"ModelLoss:" : loss, "ModelAccuracy:" : acc}

    def testDataLoop(self, outputs):
        batchDataLoss = [i["ModelLoss:"] for i in outputs]
        numRunsLoss = torch.stack(batchDataLoss).mean()
        batchDataAcc = [i["ModelAccuracy:"] for i in outputs]
        numRunsAcc = torch.stack(batchDataAcc).mean()
        return {"ModelLoss:" : numRunsLoss.item(), "ModelAccuracy:" : numRunsAcc.item()}
    
    def loopStop(self, numOfRuns, result):
        print("numOfRuns [{}], ModelLoss: {:.3f}, ModelAccuracy: {:.3f}".format(numOfRuns, result['ModelLoss:'], result['ModelAccuracy:']))


inputSize = 27648                               # 96x96 per channel, 3 channels rgb
#inputSize = 9216
numOfClasses = 10
model = stl10Model(inputSize, hiddenSize=32, outSize=numOfClasses)
cudaGpu = torch.device('cuda')   # Harcoded to use cpu atm, gpu is buggy
print(cudaGpu)
print("Cuda is available: ", torch.cuda.is_available())

def dataToGpu(data, cudaGpu):
    if isinstance(data, (list,tuple)):
        return [dataToGpu(i, cudaGpu) for i in data]
    return data.to(cudaGpu, non_blocking=True)

class gpuDataLoader():
    def __init__(self, dataLoader, cudaGpu):
        self.dataLoader = dataLoader
        self.device = cudaGpu

    def __iter__(self):
        for i in self.dataLoader:
            yield dataToGpu(i, self.device)

    def __len__(self):
        return len(self.dataLoader)

trainLoader = gpuDataLoader(trainLoader, cudaGpu)
testLoader = gpuDataLoader(testLoader, cudaGpu)
dataToGpu(model, cudaGpu)       # pass data to gpu

def testDataCheckVal(model, testLoader):
    outputs = [model.testData(batchData) for batchData in testLoader]
    return model.testDataLoop(outputs)

def run(numOfRuns, learnR, model, trainLoader, testLoader, optFunc=torch.optim.SGD):
    log = []
    opti = optFunc(model.parameters(), learnR)
    for i in range(numOfRuns):
        for dataBatch in trainLoader:
            loss = model.train(dataBatch)
            loss.backward()
            opti.step()
            opti.zero_grad()
        result = testDataCheckVal(model, testLoader)
        model.loopStop(i, result)
        log.append(result)
    return log
 
log = [testDataCheckVal(model, testLoader)]
log += run(20, 0.1, model, trainLoader, testLoader)